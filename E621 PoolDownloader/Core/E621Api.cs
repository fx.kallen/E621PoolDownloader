﻿// ------------------------------------------------------------------------------------------------
//  <copyright file="E621Api.cs"
//             company="gtaroleplay.de / fluxter.net">
//       Copyright (c) gtaroleplay.de / fluxter.net. All rights reserved.
//  </copyright>
//  <author>Marcel Kallen</author>
//  <created>04.03.2018 - 21:45</created>
// ------------------------------------------------------------------------------------------------

namespace E621_PoolDownloader.Core
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Xml.Linq;
    using Helper;
    using Models;

    public class E621Api
    {
        public IEnumerable<Pool> GetPoolsByTag(string tags)
        {
            var foundIds = new List<int>();
            foreach (var p in this.GetPostIdsByTags(tags + " inpool:true"))
            {
                var post = new Post(this, p);
                var ids = this.GetPoolIdsForPost(post);
                foreach (var id in ids)
                {
                    if (foundIds.Any(x => x == id))
                    {
                        continue;
                    }

                    foundIds.Add(id);
                    Debug.WriteLine($"Found Pool {id}");
                    yield return new Pool(this, id);
                }
            }
        }

        public IEnumerable<int> GetPoolIdsForPost(Post post)
        {
            var url = $"https://e621.net/post/show/{post.Id}";
            var data = WebClientHelper.GetE621WebClient().DownloadString(url);
            var regex = new Regex(@"\/pool\/show\/(\d+)");
            if (regex.IsMatch(data))
            {
                var ids = new List<int>();
                foreach (Match m in regex.Matches(data))
                {
                    ids.Add(Convert.ToInt32(m.Groups[1].Value));
                }

                return ids;
            }

            return null;
        }

        public SemaphoreSlim PoolDownloadSemaphore = new SemaphoreSlim(5, 5);

        public IEnumerable<int> GetPostIdsByTags(string tags)
        {
            var page = 1;
            var found = true;
            do
            {
                var url = $"https://e621.net/post/index.xml?tags={tags}&limit=50&page={page}";
                page++;
                var xml = XmlHelper.GetXmlFromUrl(url);
                foreach (var p in xml.Elements("post"))
                {
                    yield return Convert.ToInt32(p.Element("id").Value);
                }
            } while (found);
        }

        public XElement GetPoolData(int poolId)
        {
            return XmlHelper.GetXmlFromUrl($"https://e621.net/pool/show.xml?id={poolId}");
        }

        public XElement GetPostData(int postId)
        {
            return XmlHelper.GetXmlFromUrl($"https://e621.net/post/show.xml?id={postId}");
        }

        public IEnumerable<Post> GetPostsByPoolId(int poolId, Action<double> progressReport = null)
        {
            var poolData = this.GetPoolData(poolId);
            var index = 0;
            var page = 1;
            var work = true;
            var postCount = Convert.ToInt32(poolData.Attribute("post_count").Value);
            do
            {
                var posts = poolData.Element("posts").Elements("post");
                foreach (var p in posts)
                {
                    var postid = p.Element("id").Value;
                    index++;
                    progressReport?.Invoke(index * 100 / postCount);
                    yield return new Post(this, Convert.ToInt32(postid));

                }

                if (index == postCount)
                {
                    work = false;
                }
                else
                {
                    page++;
                    poolData = XmlHelper.GetXmlFromUrl($"https://e621.net/pool/show.xml?id={poolId}&page={page}");
                }
            } while (work);
        }
        
        public SemaphoreSlim PostDownloadSemaphore = new SemaphoreSlim(20, 20);

        public async Task DownloadPostsByPoolIdAsync(int poolId, string directory, Action<double> progressReport = null)
        {
            Debug.WriteLine($"Downloading Pool {poolId}");
            await this.PoolDownloadSemaphore.WaitAsync().ConfigureAwait(true); ;
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }

            var tasks = new List<Task>();
            var index = 0;
            foreach (var p in this.GetPostsByPoolId(poolId, progressReport))
            {
                PostDownloadSemaphore.WaitAsync().ConfigureAwait(true);
                Debug.WriteLine($"Downloading Post {p}");
                var url = p.FileUrl;
                var file = Path.Combine(directory, index + "." + p.Data.Element("file_ext").Value);
                var task = Task.Run(async () =>
                {
                    await WebClientHelper.GetE621WebClient().DownloadFileTaskAsync(new Uri(url), file);
                    PostDownloadSemaphore.Release();
                });

                index++;
                tasks.Add(task);
            }

            await Task.WhenAll(tasks);
            this.PoolDownloadSemaphore.Release();
        }
    }
}