﻿// ------------------------------------------------------------------------------------------------
//  <copyright file="Post.cs"
//             company="gtaroleplay.de / fluxter.net">
//       Copyright (c) gtaroleplay.de / fluxter.net. All rights reserved.
//  </copyright>
//  <author>Marcel Kallen</author>
//  <created>04.03.2018 - 22:21</created>
// ------------------------------------------------------------------------------------------------

namespace E621_PoolDownloader.Models
{
    using System.Xml.Linq;
    using Core;

    public class Post
    {
        public int Id { get; }

        public string FileUrl { get; private set; }

        public XElement Data { get; private set; }

        private E621Api Api { get; }

        public Post(E621Api api, int id, bool load = true)
        {
            this.Api = api;
            this.Id = id;
            if (load)
            {
                this.LoadData();
            }
        }

        public void LoadData()
        {
            this.Data = this.Api.GetPostData(this.Id);
            this.FileUrl = this.Data.Element("file_url").Value;
        }
    }
}