﻿// ------------------------------------------------------------------------------------------------
//  <copyright file="WebClientHelper.cs"
//             company="gtaroleplay.de / fluxter.net">
//       Copyright (c) gtaroleplay.de / fluxter.net. All rights reserved.
//  </copyright>
//  <author>Marcel Kallen</author>
//  <created>04.03.2018 - 21:32</created>
// ------------------------------------------------------------------------------------------------

namespace E621_PoolDownloader.Helper
{
    using System.Net;

    public static class WebClientHelper
    {
        public static WebClient GetE621WebClient()
        {
            var wc = new WebClient();
            wc.Headers.Add("user-agent", "E621 PoolDownloader/1.0 (by vito on e621)");
            return wc;
        }
    }
}