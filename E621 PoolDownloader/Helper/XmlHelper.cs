﻿// ------------------------------------------------------------------------------------------------
//  <copyright file="XmlHelper.cs"
//             company="gtaroleplay.de / fluxter.net">
//       Copyright (c) gtaroleplay.de / fluxter.net. All rights reserved.
//  </copyright>
//  <author>Marcel Kallen</author>
//  <created>04.03.2018 - 21:33</created>
// ------------------------------------------------------------------------------------------------

namespace E621_PoolDownloader.Helper
{
    using System.Xml.Linq;

    public static class XmlHelper
    {
        public static XElement GetXmlFromUrl(string url)
        {
            var data = WebClientHelper.GetE621WebClient().DownloadString(url);
            var elem = XElement.Parse(data);
            return elem;
        }
    }
}